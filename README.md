# Prueba técnica PHP Zinobe
Prueba técnica para aplicar al cargo de desarrollador backend PHP

## Requerimientos
- PHP 7.2 o superior
- Composer
- extensión php-curl
- MySQL 5.7

## Instalación

### 1. Clonar repositorio:
Clonar el repositorio dentro de la carpeta de localhost y luego moverse dentro de la carpeta creada:
```
git clone https://gitlab.com/DMalagonH/prueba-tecnica-zinobe.git zinobe
cd zinobe
```

### 2. Instalar dependencias con el siguiente comando:
```
composer install
```

### 3. Crear base de datos MySQL vacia

### 4. Configurar variables de entorno:    
* Copiar el archivo .env.example con el nombre .env
* Remplazar el valor de las variables incluyendo los parametros de conexión a la base de datos creada.

### 5. Generar tablas para la base de datos con el siguiente comando:
```
vendor/bin/doctrine orm:schema-tool:create
```

### 6. Crear cronjob para procesar cola de búsquedas cada x tiempo 
Ejemplo para Ubuntu:  
```
crontab -e
```
	
Agregar la siguiente linea y ajustar la ruta al proyecto. Esto ejecutará el proceso cada 5 minutos.

```
*/5 * * * * php /path/to/zinobe/bin/process_queue.php
```

**También puede ejecutar el siguiente comando para procesar la cola de búsquedas:**
	
```
php bin/process_queue.php
```

## Pruebas

### 1. Configurar variables de entorno:    
* Copiar el archivo .env.example con el nombre .env.test
* Remplazar el valor de las variables.

### 2. Ejecutar el siguiente comando para ejecutar las pruebas
```
composer run-script test
```