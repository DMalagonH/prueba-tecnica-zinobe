<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once "src/Core/Common/Database/DatabaseConnection.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

use Dmalagonh\Zinobe\Core\Common\Database\DatabaseConnection;

$DB = new DatabaseConnection;

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($DB->getEntityManager());