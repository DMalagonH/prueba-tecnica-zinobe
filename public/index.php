<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Dmalagonh\Zinobe\Core\Common\Session\Session;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/..');
$dotenv->load();

Session::init();

require_once __DIR__ . '/../src/routes.php';
