<?php
namespace Dmalagonh\Zinobe\Tests\Core\Auth\Service;

use Dmalagonh\Zinobe\Tests\AbstractTestCase;

class AuthServiceSuccessTest extends AbstractTestCase
{
	public function testCanLogin()
	{
		$signupService = $this->getContainer()->get("signup.service");
		$unique = uniqid();
		$signupData = [
			"name" => "test".$unique,
			"document" => $unique,
			"email" => $unique."@email.com",
			"country" => "CO",
			"password" => "abc123."
		];

		$signedUpUser = $signupService->create($signupData);
		$this->assertIsInt($signedUpUser->getId());

		$authService = $this->getContainer()->get("auth.service");

		$loginData = [
			"email" => $signupData["email"],
			"password" => $signupData["password"]
		];

		$loggedUser = $authService->auth($loginData);

		$this->assertIsInt($signedUpUser->getId());
		$this->assertEquals($signupData["name"], $loggedUser->getName());
		$this->assertEquals($signupData["document"], $loggedUser->getDocument());
		$this->assertEquals($signupData["email"], $loggedUser->getEmail());
		$this->assertEquals($signupData["country"], $loggedUser->getCountry());
	}
}