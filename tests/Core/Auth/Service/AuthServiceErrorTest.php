<?php
namespace Dmalagonh\Zinobe\Tests\Core\Auth\Service;

use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Tests\AbstractTestCase;

class AuthServiceErrorTest extends AbstractTestCase
{
	public function testInvalidData()
	{
		$authService = $this->getContainer()->get("auth.service");

		$data = [
			"email" => "invalidemail",
			"password" => "12"
		];

		try {
			$authService->auth($data);
		} catch (InvalidDataException $e) {
			$errors = $e->getErrors();
			$this->assertArrayHasKey("email", $errors);
			$this->assertArrayHasKey("password", $errors);
		}
	}
	
	public function testInvalidCredentials()
	{
		$authService = $this->getContainer()->get("auth.service");
		
		$data = [
			"email" => "notexists@mail.com",
			"password" => "abc1234."
		];
		
		try {
			$authService->auth($data);
		} catch (InvalidDataException $e) {
			$errors = $e->getErrors();
			$this->assertArrayHasKey("invalid_credentials", $errors);
		}
	}
}