<?php
namespace Dmalagonh\Zinobe\Tests\Core\Directory\Service;

use Dmalagonh\Zinobe\Tests\AbstractTestCase;

class DirectoryServiceSuccessTest extends AbstractTestCase
{
	public function testCanAddToQueue()
	{
		$directoryService = $this->getContainer()->get("directory.service");
		$userId = 1;
		$keywords = ["leslie", "william"];

		foreach ($keywords as $kw)
		{
			$queueItem = $directoryService->addToQueue($userId, $kw);
	
			$this->assertEquals($userId, $queueItem->getUserId());
			$this->assertEquals($kw, $queueItem->getKeywords());
		}
	}

	public function testCanGetSearchQueue()
	{
		$directoryService = $this->getContainer()->get("directory.service");
		$userId = 1;

		$queue = $directoryService->getSearchQueue($userId);

		foreach ($queue as $item) {
			$this->assertEquals($userId, $item->getUserId());
			$this->assertIsString($item->getStatus());
			$this->assertIsString($item->getKeywords());
		}
	}

	public function testCanProcessQueue()
	{
		$directoryService = $this->getContainer()->get("directory.service");

		$processed = $directoryService->processQueue();

		$this->assertGreaterThan(0, $processed);
	}

	public function testCanGetSearchResults()
	{
		$directoryService = $this->getContainer()->get("directory.service");
		$queueId = 1;

		$results = $directoryService->getSearchResults($queueId);

		foreach ($results as $result) {
			$this->assertEquals($queueId, $result->getSearchQueueId());
			$this->assertIsString($result->getFirstName());
			$this->assertIsString($result->getLastName());
			$this->assertIsString($result->getEmail());
			$this->assertIsString($result->getDocument());
			$this->assertIsString($result->getJobTitle());
			$this->assertIsString($result->getPhone());
			$this->assertIsString($result->getCountry());
			$this->assertIsString($result->getState());
			$this->assertIsString($result->getCity());
			$this->assertIsString($result->getSource());
		}
	}
}