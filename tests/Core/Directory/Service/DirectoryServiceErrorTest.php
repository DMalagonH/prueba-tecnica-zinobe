<?php
namespace Dmalagonh\Zinobe\Tests\Core\Directory\Service;

use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Tests\AbstractTestCase;

class DirectoryServiceErrorTest extends AbstractTestCase
{
	public function testInvalidSearch()
	{
		$directoryService = $this->getContainer()->get("directory.service");
		$userId = 1;
		$keywords = "";

		try {
			$directoryService->addToQueue($userId, $keywords);
		} catch (InvalidDataException $e) {
			$errors = $e->getErrors();
			$this->assertArrayHasKey("keywords", $errors);
		}
	}
}