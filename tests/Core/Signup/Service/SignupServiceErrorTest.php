<?php
namespace Dmalagonh\Zinobe\Tests\Core\Signup\Service;

use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Tests\AbstractTestCase;

class SignupServiceErrorTest extends AbstractTestCase
{
	public function testInvalidData()
	{
		$signupService = $this->getContainer()->get("signup.service");

		$data = [
			"name" => "di",
			"document" => "12",
			"email" => "invalid",
			"country" => "Colombia",
			"password" => "test"
		];

		try {
			$signupService->create($data);
		} catch (InvalidDataException $e) {
			$errors = $e->getErrors();

			$this->assertArrayHasKey("name", $errors);
			$this->assertArrayHasKey("email", $errors);
			$this->assertArrayHasKey("document", $errors);
			$this->assertArrayHasKey("country", $errors);
			$this->assertArrayHasKey("password", $errors);
		}
	}
}