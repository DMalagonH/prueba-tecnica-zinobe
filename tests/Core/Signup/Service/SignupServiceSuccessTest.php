<?php
namespace Dmalagonh\Zinobe\Tests\Core\Signup\Service;

use Dmalagonh\Zinobe\Tests\AbstractTestCase;

class SignupServiceSuccessTest extends AbstractTestCase
{
	public function testCanCreate()
	{
		$signupService = $this->getContainer()->get("signup.service");

		$unique = uniqid();
		$data = [
			"name" => "test".$unique,
			"document" => $unique,
			"email" => $unique."@email.com",
			"country" => "CO",
			"password" => "asdc12"
		];

		$user = $signupService->create($data);

		$this->assertIsInt($user->getId());
		$this->assertEquals($data["name"], $user->getName());
		$this->assertEquals($data["document"], $user->getDocument());
		$this->assertEquals($data["email"], $user->getEmail());
		$this->assertEquals($data["country"], $user->getCountry());
	}
}