<?php
namespace Dmalagonh\Zinobe\Tests;

use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Dmalagonh\Zinobe\Core\Common\Session\Session;

abstract class AbstractTestCase extends TestCase
{
	protected function setUp() :void
	{
		$this->init();
	}

	protected function init()
	{
		Session::init();
		$this->loadServices();
		$dotenv = Dotenv::createImmutable(__DIR__.'/..', '.env.test');
		$dotenv->load();
	}

	protected function getContainer()
	{
		return $this->containerBuilder;
	}

	private function loadServices()
	{
		$this->containerBuilder = new ContainerBuilder();
		$loader = new YamlFileLoader($this->containerBuilder, new FileLocator(__DIR__.'/../src/'));
		$loader->load('services.yml');
	}
}