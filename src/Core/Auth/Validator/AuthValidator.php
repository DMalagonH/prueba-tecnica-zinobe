<?php
namespace Dmalagonh\Zinobe\Core\Auth\Validator;

use Dmalagonh\Zinobe\Core\Common\Validator\AbstractValidator;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;

class AuthValidator extends AbstractValidator
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function validate(array $data): bool
	{
		$constrains = new Collection([
			'email' => [new Required(), new NotBlank(), new Type("string"), new Email()],
			'password' => [new Required(), new NotBlank(), new Type('string'), new Length(['min' => 6, 'max' => 20])]
		]);

		return $this->validateConstraints($data, $constrains);
	}
}