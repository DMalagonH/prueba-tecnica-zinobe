<?php
namespace Dmalagonh\Zinobe\Core\Auth\Service;

use Dmalagonh\Zinobe\Core\Common\Entity\User;

interface IAuthService {
	public function auth(array $data): User;
	public function logout(): void;
}