<?php
namespace Dmalagonh\Zinobe\Core\Auth\Service;

use Dmalagonh\Zinobe\Core\Auth\Validator\AuthValidator;
use Dmalagonh\Zinobe\Core\Common\Entity\User;
use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Core\Common\PasswordEncoder\IPasswordEncoder;
use Dmalagonh\Zinobe\Core\Common\Repository\IUserRepository;
use Dmalagonh\Zinobe\Core\Common\Session\ISession;

class AuthService implements IAuthService
{
	private $userRepository;
	private $validator;
	private $passwordEncoder;
	private $session;

	public function __construct(
		IUserRepository $userRepository,
		AuthValidator $validator,
		IPasswordEncoder $passwordEncoder,
		ISession $session
	) {
		$this->userRepository = $userRepository;
		$this->validator = $validator;
		$this->passwordEncoder = $passwordEncoder;
		$this->session = $session;
	}

	public function auth(array $data): User 
	{
		$valid = $this->validator->validate($data);
		if (!$valid) throw new InvalidDataException($this->validator->getErrorMessages());

		$user = $this->userRepository->auth($data["email"], $this->passwordEncoder->encode($data["password"], $data["email"]));
		if (!$user) throw new InvalidDataException(["invalid_credentials" => "Invalid credentials"]);

		return $user;
	}

	public function logout(): void 
	{
		$this->session::close();
	}
}