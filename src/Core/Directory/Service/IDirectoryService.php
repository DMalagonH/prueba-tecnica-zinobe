<?php
namespace Dmalagonh\Zinobe\Core\Directory\Service;

use Dmalagonh\Zinobe\Core\Common\Entity\SearchQueue;

interface IDirectoryService
{
	public function getSearchQueue(int $userId): ?array;
	public function getSearchResults(int $searchQueueId): ?array;
	public function addToQueue(int $userId, string $keywords): SearchQueue;
	public function processQueue(): int;
}