<?php
namespace Dmalagonh\Zinobe\Core\Directory\Service;

use Dmalagonh\Zinobe\Core\Common\Entity\SearchQueue;
use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Core\Directory\DataSource\DataSourcePrimary;
use Dmalagonh\Zinobe\Core\Directory\DataSource\DataSourceSecondary;
use Dmalagonh\Zinobe\Core\Directory\DataSource\ICustomerData;
use Dmalagonh\Zinobe\Core\Directory\Repository\ISearchQueueRepository;
use Dmalagonh\Zinobe\Core\Directory\Repository\ISearchResultRepository;

class DirectoryService implements IDirectoryService
{
	private $queueRepository;
	private $resultRepository;
	private $customerData;
	private $primaryDatasourceEndpoint;
	private $secondaryDatasourceEndpoint;
	private $queueProcessLimit;

	public function __construct(
		ISearchQueueRepository $queueRepository, 
		ISearchResultRepository $resultRepository,
		ICustomerData $customerData
	) {
		$this->queueRepository = $queueRepository;
		$this->resultRepository = $resultRepository;
		$this->customerData = $customerData;
		$this->primaryDatasourceEndpoint = $_ENV["PRI_DATASOURCE_ENDPOINT"];
		$this->secondaryDatasourceEndpoint = $_ENV["SEC_DATASOURCE_ENDPOINT"];
		$this->queueProcessLimit = $_ENV["QUEUE_PROCESS_ITEMS"];
		$this->setDataSources();
	}

	public function getSearchQueue(int $userId): ?array
	{
		$results = $this->queueRepository->findByUserId($userId);
		return $results;
	}

	public function addToQueue(int $userId, string $keywords): SearchQueue
	{
		if (empty($keywords)) throw new InvalidDataException([ "keywords" => "This value should not be blank." ]);

		$queueItem = new SearchQueue();
		$queueItem
			->setUserId($userId)
			->setStatus("queued")
			->setKeywords($keywords)
			->setCreatedAt(new \DateTime());

		$this->queueRepository->create($queueItem);

		return $queueItem;
	}

	public function processQueue(): int
	{
		$processed = 0;
		// Obtener x primeros items pendientes en la cola
		$queue = $this->queueRepository->getQueued($this->queueProcessLimit);
		
		foreach ($queue as $queueItem) {
			// Marcar item como procesando
			$this->queueRepository->updateStatus($queueItem->getId(), "processing");

			$results = $this->search($queueItem->getKeywords());
	
			// Almacenar resultados en la base datos
			foreach ($results as $result) {
				$result->setSearchQueueId($queueItem->getId());
				$this->resultRepository->create($result);
			}
	
			// Marcar item como finalizado
			$this->queueRepository->updateStatus($queueItem->getId(), "done", count($results));
			$processed++;
		}

		return $processed;
	}

	public function getSearchResults(int $searchQueueId): ?array
	{
		$results = $this->resultRepository->findBySearchQueueId($searchQueueId);
		return $results;
	}

	protected function search(string $keywords): ?array
	{
		$results = $this->customerData->search($keywords);
		return $results;
	}

	protected function setDataSources(): void
	{
		$dataSourcePrimary = new DataSourcePrimary($this->primaryDatasourceEndpoint);
		$dataSourceSecondary = new DataSourceSecondary($this->secondaryDatasourceEndpoint);

		$this->customerData->addDataSource($dataSourcePrimary);
		$this->customerData->addDataSource($dataSourceSecondary);
	}
}