<?php
namespace Dmalagonh\Zinobe\Core\Directory\Repository;

use Dmalagonh\Zinobe\Core\Common\Entity\SearchQueue;

interface ISearchQueueRepository
{
	public function create(SearchQueue $searchQueue): SearchQueue;
	public function updateStatus(int $id, string $status, ?int $results = null, ?string $errorMessage = null): bool;
	public function findByUserId(int $userId): ?array;
	public function getQueued(int $limit): ?array;
}