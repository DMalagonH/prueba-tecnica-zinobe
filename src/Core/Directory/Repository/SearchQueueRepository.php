<?php
namespace Dmalagonh\Zinobe\Core\Directory\Repository;

use Dmalagonh\Zinobe\Core\Common\Database\IDatabaseConnection;
use Dmalagonh\Zinobe\Core\Common\Entity\SearchQueue;

class SearchQueueRepository implements ISearchQueueRepository
{
	private $db;

	public function __construct(IDatabaseConnection $db)
	{
		$this->db = $db;
	}

	public function create(SearchQueue $searchQueue): SearchQueue
	{
		$this->db->save($searchQueue);
		return $searchQueue;
	}

	public function updateStatus(int $id, string $status, ?int $results = null, ?string $errorMessage = null): bool
	{
		$finder = $this->db->getFinder(SearchQueue::class);
		$searchQueue = $finder->findOneById($id);

		if ($searchQueue) {
			$searchQueue->setStatus($status);
			$searchQueue->setUpdatedAt(new \DateTime());
			$searchQueue->setResults($results);
			$searchQueue->setErrorMessage($errorMessage);
			$this->db->save($searchQueue);
			return true;
		}

		return false;
	}

	public function findByUserId(int $userId): ?array
	{
		$finder = $this->db->getFinder(SearchQueue::class);
		$results = $finder->findByUserId($userId, [ "id" => "desc" ]);
		return $results;
	}

	public function getQueued(int $limit): ?array
	{
		$finder = $this->db->getFinder(SearchQueue::class);
		$results = $finder->findByStatus("queued", null, $limit);
		return $results;
	}

}