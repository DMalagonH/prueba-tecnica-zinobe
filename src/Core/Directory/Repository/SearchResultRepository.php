<?php
namespace Dmalagonh\Zinobe\Core\Directory\Repository;

use Dmalagonh\Zinobe\Core\Common\Database\IDatabaseConnection;
use Dmalagonh\Zinobe\Core\Common\Entity\SearchResult;

class SearchResultRepository implements ISearchResultRepository
{
	private $db;

	public function __construct(IDatabaseConnection $db)
	{
		$this->db = $db;
	}

	public function create(SearchResult $result): SearchResult
	{
		$this->db->save($result);
		return $result;
	}

	public function findBySearchQueueId(int $searchQueueId): ?array
	{
		$finder = $this->db->getFinder(SearchResult::class);
		$results = $finder->findBySearchQueueId($searchQueueId);
		return $results;
	}
}