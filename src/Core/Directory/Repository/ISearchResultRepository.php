<?php
namespace Dmalagonh\Zinobe\Core\Directory\Repository;

use Dmalagonh\Zinobe\Core\Common\Entity\SearchResult;

interface ISearchResultRepository
{
	public function create(SearchResult $result): SearchResult;
	public function findBySearchQueueId(int $searchQueueId): ?array;
}