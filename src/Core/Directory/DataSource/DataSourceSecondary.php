<?php
namespace Dmalagonh\Zinobe\Core\Directory\DataSource;

use Dmalagonh\Zinobe\Core\Common\Entity\SearchResult;

class DataSourceSecondary extends AbstractDataSource
{
	private $sourceName;

	public function __construct(string $endpoint)
	{
		parent::__construct($endpoint);
		$this->sourceName = "secondary";
	}

	public function search(string $keywords): ?array
	{
		$keywords = strtolower($keywords);
		$data =  $this->makeRequest();

		$results = array_filter($data["objects"], function ($i) use ($keywords) {
			return (strtolower($i["first_name"]) == $keywords || strtolower($i["last_name"]) == $keywords || strtolower($i["email"]) == $keywords);
		});

		$results = (count($results)) ? $this->mapResults($results): [];

		return $results;
	}

	protected function mapResults(array $results): array
	{
		$mapped = array_map(function($result){
			$customer =  new SearchResult();
			$customer
				->setSource($this->sourceName)
				->setId($result["id"])
				->setFirstName($result["first_name"])
				->setLastName($result["last_name"])
				->setEmail($result["email"])
				->setDocument($result["document"])
				->setJobTitle($result["job_title"])
				->setPhone($result["phone_number"])
				->setCountry($result["country"])
				->setState($result["state"])
				->setCity($result["city"])
			;

			return $customer;
		}, $results);

		return $mapped;
	}
}