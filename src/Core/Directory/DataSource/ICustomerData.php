<?php
namespace Dmalagonh\Zinobe\Core\Directory\DataSource;

interface ICustomerData
{
	public function search(string $keywords): ?array;
	public function addDataSource(AbstractDataSource $dataSource): void;
}