<?php
namespace Dmalagonh\Zinobe\Core\Directory\DataSource;

class CustomerData implements ICustomerData
{
	private $dataSources = [];

	public function search(string $keywords): ?array
	{
		$results = [];
		
		foreach ($this->dataSources as $dataSource) {
			$results = array_merge($results, $dataSource->search($keywords));
		}

		return $results;
	}

	public function addDataSource(AbstractDataSource $dataSource): void
	{
		array_push($this->dataSources, $dataSource);
	}
}