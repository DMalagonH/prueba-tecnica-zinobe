<?php
namespace Dmalagonh\Zinobe\Core\Directory\DataSource;

abstract class AbstractDataSource
{
	private $endpoint;

	public function __construct(string $endpoint) 
	{
		$this->endpoint = $endpoint;
	}

	abstract public function search(string $keywords): ?array;

	protected function makeRequest()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$response = curl_exec($ch);

		if (curl_errno($ch)) {
			throw new \Exception(curl_error($ch));
		}

		curl_close($ch);

		return json_decode($response, true);
	}
}