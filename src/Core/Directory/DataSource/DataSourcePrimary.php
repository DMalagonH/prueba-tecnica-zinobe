<?php
namespace Dmalagonh\Zinobe\Core\Directory\DataSource;

use Dmalagonh\Zinobe\Core\Common\Entity\SearchResult;

class DataSourcePrimary extends AbstractDataSource
{
	private $sourceName;

	public function __construct(string $endpoint)
	{
		parent::__construct($endpoint);
		$this->sourceName = "primary";
	}

	public function search(string $keywords): ?array
	{
		$keywords = strtolower($keywords);
		$data =  $this->makeRequest();

		$results = array_filter($data["objects"], function ($i) use ($keywords) {
			return (strtolower($i["primer_nombre"]) == $keywords || strtolower($i["apellido"]) == $keywords || strtolower($i["correo"]) == $keywords);
		});

		$results = (count($results)) ? $this->mapResults($results): [];

		return $results;
	}

	protected function mapResults(array $results): array
	{
		$mapped = array_map(function($result){
			$customer =  new SearchResult();
			$customer
				->setSource($this->sourceName)
				->setId($result["id"])
				->setFirstName($result["primer_nombre"])
				->setLastName($result["apellido"])
				->setEmail($result["correo"])
				->setDocument($result["cedula"])
				->setJobTitle($result["cargo"])
				->setPhone($result["telefono"])
				->setCountry($result["pais"])
				->setState($result["departamento"])
				->setCity($result["ciudad"])
			;

			return $customer;
		}, $results);

		return $mapped;
	}
}