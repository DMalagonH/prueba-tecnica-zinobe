<?php
namespace Dmalagonh\Zinobe\Core\Common\PasswordEncoder;

interface IPasswordEncoder
{
	public function encode(string $value, ?string $salt = null): string;
}