<?php
namespace Dmalagonh\Zinobe\Core\Common\PasswordEncoder;

use Dmalagonh\Zinobe\Core\Common\Entity\User;

class PasswordEncoder implements IPasswordEncoder
{
	public function encode(string $value, ?string $salt = null): string
	{
		return hash('sha256', $salt.$value);
	}
}