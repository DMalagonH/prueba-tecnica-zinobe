<?php
namespace Dmalagonh\Zinobe\Core\Common\Database;

interface IDatabaseConnection
{
	public function getFinder(string $entityName);
	public function persist(object $object);
	public function commit();
	public function save(object $object);
}