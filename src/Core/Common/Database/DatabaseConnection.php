<?php
namespace Dmalagonh\Zinobe\Core\Common\Database;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Exception;

class DatabaseConnection implements IDatabaseConnection
{
	private $em;

	public function __construct()
	{
		$isDevMode = (strtolower($_ENV["ENV"]) != 'prod') ? true : false;
		$paths = [ __DIR__ . "/../Entity" ];
		$params = array(
			'driver'		=> $_ENV["DBDRIVER"],
			'host'			=> $_ENV["DBHOST"],
			'dbname'		=> $_ENV["DBNAME"],
			'user'			=> $_ENV["DBUSER"],
			'password'	=> $_ENV["DBPASS"],
		);
		$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
		$this->em = EntityManager::create($params, $config);
	}

	public function getEntityManager()
	{
		return $this->em;
	}

	public function getFinder(string $entityName)
	{
		try {
			return $this->em->getRepository($entityName);
		} catch (Exception $e) {
			echo $e->getMessage();
			die;
		}
	}

	public function persist(object $object)
	{
		$this->em->persist($object);
	}

	public function commit()
	{
		$this->em->flush();
	}

	public function save(object $object)
	{
		$this->persist($object);
		$this->commit();
	}
}