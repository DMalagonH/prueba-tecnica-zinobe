<?php
namespace Dmalagonh\Zinobe\Core\Common\Entity;

/**
 * @Entity
 * @Table(name="user")
 */
class User
{
	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 */
	private $id;

	/**
	 * @Column(type="string", length=45)
	 */
	private $name;

	/**
	 * @Column(type="string", length=45, unique=true)
	 */
	private $document;

	/**
	 * @Column(type="string", length=45, unique=true)
	 */
	private $email;

	/**
	 * @Column(type="string", length=2)
	 */
	private $country;

	/**
	 * @Column(type="text")
	 */
	private $password;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getDocument(): string
	{
		return $this->document;
	}

	public function setDocument(string $document): self
	{
		$this->document = $document;

		return $this;
	}

	public function getEmail(): string
	{
		return $this->email;
	}

	public function setEmail(string $email): self
	{
		$this->email = $email;

		return $this;
	}

	public function getCountry(): string
	{
		return $this->country;
	}

	public function setCountry(string $country): self
	{
		$this->country = $country;

		return $this;
	}

	public function setPassword(string $password): self
	{
		$this->password = $password;

		return $this;
	}
}