<?php
namespace Dmalagonh\Zinobe\Core\Common\Entity;

/**
 * @Entity
 * @Table(name="search_result")
 */
class SearchResult
{
	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 */
	private $id;

	/**
	 * @Column(type="integer")
	 */
	private $searchQueueId;

	/**
	 * @Column(type="string")
	 */
	private $firstName;

	/**
	 * @Column(type="string")
	 */
	private $lastName;

	/**
	 * @Column(type="string")
	 */
	private $email;

	/**
	 * @Column(type="string")
	 */
	private $document;

	/**
	 * @Column(type="string")
	 */
	private $jobTitle;

	/**
	 * @Column(type="string")
	 */
	private $phone;

	/**
	 * @Column(type="string")
	 */
	private $country;

	/**
	 * @Column(type="string")
	 */
	private $state;

	/**
	 * @Column(type="string")
	 */
	private $city;

	/**
	 * @Column(type="date", nullable=true)
	 */
	private $birthdate;

	/**
	 * @Column(type="string")
	 */
	private $source;

	public function getId(): int
	{
		return $this->id;
	}

	public function setId(int $id): self
	{
		$this->id = $id;

		return $this;
	}

	public function getSearchQueueId(): int
	{
		return $this->searchQueueId;
	}

	public function setSearchQueueId(int $searchQueueId): self
	{
		$this->searchQueueId = $searchQueueId;

		return $this;
	}

	public function getFirstName(): string
	{
		return $this->firstName;
	}

	public function setFirstName(string $firstName): self
	{
		$this->firstName = $firstName;

		return $this;
	}

	public function getLastName(): string
	{
		return $this->lastName;
	}

	public function setLastName(string $lastName): self
	{
		$this->lastName = $lastName;

		return $this;
	}

	public function getEmail(): string
	{
		return $this->email;
	}

	public function setEmail(string $email): self
	{
		$this->email = $email;

		return $this;
	}

	public function getDocument(): string
	{
		return $this->document;
	}

	public function setDocument(string $document): self
	{
		$this->document = $document;

		return $this;
	}

	public function getJobTitle(): string
	{
		return $this->jobTitle;
	}

	public function setJobTitle(string $jobTitle): self
	{
		$this->jobTitle = $jobTitle;

		return $this;
	}

	public function getPhone(): string
	{
		return $this->phone;
	}

	public function setPhone(string $phone): self
	{
		$this->phone = $phone;

		return $this;
	}

	public function getCountry(): string
	{
		return $this->country;
	}

	public function setCountry(string $country): self
	{
		$this->country = $country;

		return $this;
	}

	public function getState(): string
	{
		return $this->state;
	}

	public function setState(string $state): self
	{
		$this->state = $state;

		return $this;
	}

	public function getCity(): string
	{
		return $this->city;
	}

	public function setCity(string $city): self
	{
		$this->city = $city;

		return $this;
	}

	public function getBirthdate(): ?\DateTimeInterface
	{
		return $this->birthdate;
	}

	public function setBirthdate(?\DateTimeInterface $birthdate): self
	{
		$this->birthdate = $birthdate;

		return $this;
	}

	public function getSource(): string
	{
		return $this->source;
	}

	public function setSource(string $source): self
	{
		$this->source = $source;

		return $this;
	}
}
