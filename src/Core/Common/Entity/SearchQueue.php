<?php
namespace Dmalagonh\Zinobe\Core\Common\Entity;

/**
 * @Entity
 * @Table(name="search_queue")
 */
class SearchQueue
{
	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 */
	private $id;

	/**
	 * @Column(type="integer")
	 */
	private $userId;

	/**
	 * @Column(type="string")
	 */
	private $status;

	/**
	 * @Column(type="string")
	 */
	private $keywords;

	/**
	 * @Column(type="integer", nullable=true)
	 */
	private $results;

	/**
	 * @Column(type="datetime")
	 */
	private $createdAt;

	/**
	 * @Column(type="datetime", nullable=true)
	 */
	private $updatedAt;

	/**
	 * @Column(type="string", nullable=true)
	 */
	private $errorMessage;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getUserId(): ?int
	{
		return $this->userId;
	}

	public function setUserId(int $userId): self
	{
		$this->userId = $userId;

		return $this;
	}

	public function getStatus(): ?string
	{
		return $this->status;
	}

	public function setStatus(string $status): self
	{
		$this->status = $status;

		return $this;
	}

	public function getKeywords(): ?string
	{
		return $this->keywords;
	}

	public function setKeywords(string $keywords): self
	{
		$this->keywords = $keywords;

		return $this;
	}

	public function getResults(): ?int
	{
		return $this->results;
	}

	public function setResults(?int $results): self
	{
		$this->results = $results;

		return $this;
	}

	public function getCreatedAt(): ?\DateTimeInterface
	{
		return $this->createdAt;
	}

	public function setCreatedAt(\DateTimeInterface $createdAt): self
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	public function getUpdatedAt(): ?\DateTimeInterface
	{
		return $this->updatedAt;
	}

	public function getErrorMessage(): ?string
	{
		return $this->errorMessage;
	}

	public function setErrorMessage(?string $errorMessage): self
	{
		$this->errorMessage = $errorMessage;

		return $this;
	}
}