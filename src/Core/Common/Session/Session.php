<?php
namespace Dmalagonh\Zinobe\Core\Common\Session;

use duncan3dc\Sessions\Session as SessionManager;
use Dmalagonh\Zinobe\Core\Common\Entity\User;

class Session implements ISession
{
	public static function init(): void
	{
		SessionManager::name("zinobe-session");
	}

	public static function start(User $user): void
	{
		SessionManager::set("user", [
			"id"			=> $user->getId(),
			"name"		=> $user->getName(),
			"document"=> $user->getDocument(),
			"email"		=> $user->getEmail(),
			"country"	=> $user->getCountry()
		]);
	}

	public static function getUser()
	{
		return SessionManager::get("user");
	}

	public static function close(): void
	{
		SessionManager::destroy();
	}
}