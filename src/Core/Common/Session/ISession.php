<?php
namespace Dmalagonh\Zinobe\Core\Common\Session;

use Dmalagonh\Zinobe\Core\Common\Entity\User;

interface ISession
{
	public static function init(): void;
	public static function start(User $user): void;
	public static function getUser();
	public static function close(): void;
}