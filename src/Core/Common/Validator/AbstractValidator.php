<?php
namespace Dmalagonh\Zinobe\Core\Common\Validator;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Validation;

class AbstractValidator
{
	protected $validator;
	protected $errorMessages;

	public function __construct()
	{
		$this->validator = Validation::createValidator();
	}

	public function getErrorMessages(): ?array
	{
		return $this->errorMessages;
	}

	protected function validateConstraints(array $data, Collection $constraints): bool
	{
		$validate = false; 

		if (is_array($data)) {
			$errors = $this->validator->validate($data, $constraints);

			if (!count($errors)) {
				$validate = true;
			} else {
				$this->setErrorMessages($errors);
			}
		}

		return $validate;
	}

	protected function setErrorMessages(object $errors): void
	{
		$arrErr = [];
		if (count($errors)) {
			foreach ($errors as $e) {
				$key = str_replace(['[', ']'], "", str_replace('][', '.', $e->getPropertyPath()));
				$arrErr[$key] = $e->getMessage();
			}
		}
		$this->errorMessages = $arrErr;
	}
}