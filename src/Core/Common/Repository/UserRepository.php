<?php
namespace Dmalagonh\Zinobe\Core\Common\Repository;

use Dmalagonh\Zinobe\Core\Common\Database\IDatabaseConnection;
use Dmalagonh\Zinobe\Core\Common\Entity\User;

class UserRepository implements IUserRepository
{
	private $db;

	public function __construct(IDatabaseConnection $db)
	{
		$this->db = $db;
	}

	public function create(User $user): User
	{
		$this->db->save($user);
		return $user;
	}
	
	public function auth(string $email, string $password): ?User
	{
		$finder = $this->db->getFinder(User::class);
		$result = $finder->findOneBy([ "email" => $email, "password" => $password ]);
		return $result;
	}

	public function search(string $keywords): ?array
	{
		return [];
	}
}