<?php
namespace Dmalagonh\Zinobe\Core\Common\Repository;

use Dmalagonh\Zinobe\Core\Common\Exception\NotFoundException;
use Exception;

class CountryRepository implements ICountryRepository
{
	private $endpoint;

	public function __construct()
	{
		$this->endpoint = $_ENV["COUNTRY_LIST_ENDPOINT"];
	}

	public function findAll(): ?array
	{
		$countries = [];
		try {
			$countries = $this->makeRequest($this->endpoint);
		} catch(Exception $e) {
			throw new NotFoundException("countries not found");
		}
		
		return $countries;
	}

	protected function makeRequest(string $endpoint)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$response = curl_exec($ch);

		if (curl_errno($ch)) {
			throw new \Exception(curl_error($ch));
		}

		curl_close($ch);

		return json_decode($response, true);
	}

}