<?php
namespace Dmalagonh\Zinobe\Core\Common\Repository;

interface ICountryRepository
{
	public function findAll(): ?array;
}