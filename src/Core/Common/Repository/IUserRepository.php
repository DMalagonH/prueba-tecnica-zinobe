<?php
namespace Dmalagonh\Zinobe\Core\Common\Repository;

use Dmalagonh\Zinobe\Core\Common\Entity\User;

interface IUserRepository
{
	public function create(User $user): User;
	public function auth(string $email, string $password): ?User;
	public function search(string $keywords): ?array;
}