<?php
namespace Dmalagonh\Zinobe\Core\Signup\Validator;

use Dmalagonh\Zinobe\Core\Common\Validator\AbstractValidator;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;

class SignupValidator extends AbstractValidator
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function validate(array $data): bool
	{
		$constrains = new Collection([
			'name' => [new Required(), new NotBlank(), new Type("string"), new Length(['min' => 3, 'max' => 100])],
			'email' => [new Required(), new NotBlank(), new Type("string"), new Email()],
			'document' => [new Required(), new NotBlank(), new Type("string"), new Length(['min' => 3, 'max' => 20])],
			'country' => [new Required(), new NotBlank(), new Type('string'), new Length(['min' => 2, 'max' => 2]), new Regex(['pattern' => '/^[A-Z]{2}$/'])], // ISO-3166-2
			'password' => [new Required(), new NotBlank(), new Type('string'), new Length(['min' => 6, 'max' => 20]), new Regex(['pattern' => '/^\S*(?=\S*[\d])\S*$/'])] // longitud de 6 a 20 cararacteres, cualquier carácter válido con al menos 1 dígito
		]);

		return $this->validateConstraints($data, $constrains);
	}
}