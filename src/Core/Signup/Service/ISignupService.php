<?php
namespace Dmalagonh\Zinobe\Core\Signup\Service;

use Dmalagonh\Zinobe\Core\Common\Entity\User;

interface ISignupService {
	public function create(array $data): User;
}