<?php
namespace Dmalagonh\Zinobe\Core\Signup\Service;

use Dmalagonh\Zinobe\Core\Common\Entity\User;
use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Core\Common\PasswordEncoder\IPasswordEncoder;
use Dmalagonh\Zinobe\Core\Common\Repository\IUserRepository;
use Dmalagonh\Zinobe\Core\Signup\Validator\SignupValidator;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class SignupService implements ISignupService
{
	private $userRepository;
	private $validator;
	private $passwordEncoder;

	public function __construct(
		IUserRepository $userRepository, 
		SignupValidator $validator,
		IPasswordEncoder $passwordEncoder
	) {
		$this->userRepository = $userRepository;
		$this->validator = $validator;
		$this->passwordEncoder = $passwordEncoder;
	}

	public function create(array $data): User
	{
		$valid = $this->validator->validate($data);
		if (!$valid) throw new InvalidDataException($this->validator->getErrorMessages());

		$user = $this->buildUser($data);
		
		try {
			$user = $this->userRepository->create($user);
		} catch (UniqueConstraintViolationException $e) {
			throw new InvalidDataException([ "duplicated" => "Already exists an user with your email or document" ]);
		}

		return $user;
	}

	protected function buildUser(array $data): User 
	{
		$user = new User();
		
		$user
			->setName($data["name"])
			->setEmail($data["email"])
			->setDocument($data["document"])
			->setCountry($data["country"])
			->setPassword($this->passwordEncoder->encode($data["password"], $data["email"]));

		return $user;
	}
}