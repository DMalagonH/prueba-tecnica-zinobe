<?php

use Dmalagonh\Zinobe\Controller\DirectoryController;
use Dmalagonh\Zinobe\Controller\IndexController;
use Dmalagonh\Zinobe\Controller\LoginController;
use Dmalagonh\Zinobe\Controller\SignupController;
use Dmalagonh\Zinobe\Core\Common\Session\Session;

$router = new \Mezon\Router\Router();
$loader = new Twig\Loader\FilesystemLoader(__DIR__.'/../templates');
$twig = new Twig\Environment($loader);

$router->addRoute('/', function() use ($twig) {
	publicZoneMiddleware();
	echo $twig->render('index.html.twig', buildTemplateParams(IndexController::index()));
}, 'GET');

$router->addRoute('/registro', function() use ($twig) {
	publicZoneMiddleware();
	$controller = new SignupController();
	echo $twig->render('signup/form.html.twig', buildTemplateParams($controller->signup()));
}, 'GET');

$router->addRoute('/registro', function() use ($twig) {
	publicZoneMiddleware();
	$controller = new SignupController();
	echo $twig->render('signup/form.html.twig', buildTemplateParams($controller->signup($_POST)));
}, 'POST' );

$router->addRoute('/login', function() use ($twig) {
	publicZoneMiddleware();
	$controller = new LoginController();
	echo $twig->render('login/form.html.twig', buildTemplateParams($controller->login()));
}, 'GET');

$router->addRoute('/login', function() use ($twig) {
	publicZoneMiddleware();
	$controller = new LoginController();
	echo $twig->render('login/form.html.twig', buildTemplateParams($controller->login($_POST)));
}, 'POST' );

$router->addRoute('/logout', function() {
	privateZoneMiddleware();
	$controller = new LoginController();
	$controller->logout();
}, 'GET' );

$router->addRoute('/directorio', function() use ($twig) {
	privateZoneMiddleware();
	$controller = new DirectoryController();
	echo $twig->render('directory/queue.html.twig', buildTemplateParams($controller->queue()));
}, 'GET');

$router->addRoute('/directorio', function() use ($twig) {
	privateZoneMiddleware();
	$controller = new DirectoryController();
	echo $twig->render('directory/queue.html.twig', buildTemplateParams($controller->queue($_POST)));
}, 'POST');

$router->addRoute('/directorio/resultados/[i:queue_id]', function($route, $parameters) use ($twig) {
	privateZoneMiddleware();
	$controller = new DirectoryController();
	echo $twig->render('directory/results.html.twig', buildTemplateParams($controller->resultList($parameters["queue_id"])));
}, 'GET');

$path = str_replace('/index.php', '', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$path = str_replace($_ENV["PUBLIC"], '', $path);
$path = ($path == "/") ? '' : $path;
$router->callRoute($path);

function buildTemplateParams($params) {
	$params["global"] = [
		"user" => Session::getUser(),
		"host" => $_ENV["HOST"] . $_ENV["PUBLIC"]
	];
	
	return $params;
}

function privateZoneMiddleware(): void
{
	$loginUrl = $_ENV["HOST"] . $_ENV["PUBLIC"] . "/login";
	if (!Session::getUser()) header("Location: ". $loginUrl);
}

function publicZoneMiddleware(): void
{
	$homeUrl = $_ENV["HOST"] . $_ENV["PUBLIC"] . "/directorio";
	if (Session::getUser()) header("Location: " . $homeUrl);
}