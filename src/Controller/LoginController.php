<?php
namespace Dmalagonh\Zinobe\Controller;

use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Core\Common\Session\Session;

class LoginController extends AbstractController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function login(?array $requestData = null)
	{
		$response = [
			"formData" => $requestData
		];

		$authService = $this->getContainer()->get("auth.service");

		// POST Request
		if ($requestData) {
			try {
				$user = $authService->auth($requestData);
				Session::start($user);
				$homeUrl = $_ENV["HOST"] . $_ENV["PUBLIC"] . "/directorio";
				header("Location: " . $homeUrl);
			} catch (InvalidDataException $e) {
				$response["formErrors"] = $e->getErrors();
			}
		}

		return $response;
	}

	public function logout()
	{
		$authService = $this->getContainer()->get("auth.service");
		$authService->logout();
		$rootUrl = $_ENV["HOST"] . $_ENV["PUBLIC"];
		header("Location: " .$rootUrl);
	}
}