<?php
namespace Dmalagonh\Zinobe\Controller;

use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Core\Common\Session\Session;

class DirectoryController extends AbstractController
{
	public function queue(?array $requestData = null)
	{
		$response = [];

		$directoryService = $this->getContainer()->get("directory.service");
		$user = Session::getUser();

		if ($requestData) {
			try {
				$directoryService->addToQueue($user["id"], $requestData["keywords"]);
				$response["successMessage"] = "Solicitud de búsqueda agregada";
			} catch (InvalidDataException $e) {
				$response["formErrors"] = $e->getErrors();
			}
		}

		$queue = $directoryService->getSearchQueue($user["id"]);
		$response["queue"] = $queue;

		return $response;
	}

	public function resultList(int $searchQueueId) 
	{
		$directoryService = $this->getContainer()->get("directory.service");
		$response["results"] = $directoryService->getSearchResults($searchQueueId);

		return $response;
	}
}