<?php
namespace Dmalagonh\Zinobe\Controller;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

abstract class AbstractController
{
	private $containerBuilder;

	public function __construct()
	{
		$this->loadServices();
	}

	public function getContainer()
	{
		return $this->containerBuilder;
	}

	private function loadServices()
	{
		$this->containerBuilder = new ContainerBuilder();
		$loader = new YamlFileLoader($this->containerBuilder, new FileLocator(__DIR__.'/../'));
		$loader->load('services.yml');
	}

}