<?php
namespace Dmalagonh\Zinobe\Controller;

use Dmalagonh\Zinobe\Core\Common\Exception\InvalidDataException;
use Dmalagonh\Zinobe\Core\Common\Exception\NotFoundException;
use Dmalagonh\Zinobe\Core\Common\Session\Session;

class SignupController extends AbstractController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function signup(?array $requestData = null)
	{
		$response = [
			"formData" => $requestData
		];

		$signupService = $this->getContainer()->get('signup.service');

		$response["countries"] = $this->getCountries();

		// POST Request
		if ($requestData) {
			try {
				$user = $signupService->create($requestData);
				Session::start($user);
				$homeUrl = $_ENV["HOST"] . $_ENV["PUBLIC"] . "/directorio";
				header("Location:" . $homeUrl);
			} catch (InvalidDataException $e) {
				$response["formErrors"] = $e->getErrors();
			} catch (\Exception $e) {
				var_dump($e->getMessage());
			}
		}

		return $response;
	}

	private function getCountries(): array
	{
		try {
			$countryRepo = $this->getContainer()->get("country.repository");
			$countries = $countryRepo->findAll();
		} catch (NotFoundException $e) {
			$countries = [];
		}

		return $countries;
	}
}